const fs = require('fs');
const path = require('path');

class Card {

    static async add(course){
        const card = await Card.fetch();

        const inx = card.courses.findIndex(el => el.id === course.id);
        const candidate = card.courses[inx];

        if(candidate){
            candidate.count++;
        }else{
            course.count = 1;
            card.courses.push(course);
        }

        card.price += +course.price;

        return new Promise((resolve, reject)=>{
            fs.writeFile(path.join(__dirname, '..', 'data', 'card.json'), JSON.stringify(card), (err)=>{
                if(err){
                    reject(err);
                }else{
                    resolve();
                }
            })
        })
    };

    static async fetch(){
        return new Promise((resolve, reject)=>{
            fs.readFile(path.join(__dirname, '..', 'data', 'card.json'), 'utf-8', (err, data)=>{
                if(err){
                    reject(err);
                }else{
                    resolve(JSON.parse(data));
                }
            })
        })
    };

    static async remove(id){
        const card = await Card.fetch();
        const inx = card.courses.findIndex(el => el.id === id);
        const course = card.courses[inx];

        if(course.count == 1){
            card.courses = card.courses.filter(el => el.id !== id);
        }else{
            card.courses[inx].count--;
        }
        card.price -= course.price;

        return new Promise((resolve, reject)=>{
            fs.writeFile(path.join(__dirname, '..', 'data', 'card.json'), JSON.stringify(card), (err)=>{
                if(err){
                    reject(err);
                }else{
                    resolve(card);
                }
            })
        })
    }


}

module.exports = Card;